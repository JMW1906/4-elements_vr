﻿using UnityEngine;
using System.Collections;

//Um das Terrain zu bewegen muss es einem GameObject hinzugefügt werden und das Spiel 1 mal gestartet werden!
public class MoveTerrain : MonoBehaviour 
{
	public int PixelsToMove = 50;
	public Direction directionToMove;

	Terrain terr;
	int hmWidth;
	int hmHeight;

	void Start () {
		terr = Terrain.activeTerrain;
		hmWidth = terr.terrainData.heightmapWidth;
		hmHeight = terr.terrainData.heightmapHeight;
		Terrain.activeTerrain.heightmapMaximumLOD = 0;
		Debug.Log("Heightmap Width = "+hmWidth);
		Debug.Log("Heightmap Height = "+hmHeight);


		float[,] heights = terr.terrainData.GetHeights(0,0,hmWidth,hmHeight);
		switch(directionToMove)
		{
		case Direction.NegativeZ:
			for (int i=PixelsToMove; i<hmWidth; i++)
			{
				for (int j=0; j<hmHeight; j++)
				{
					heights[i-PixelsToMove,j] = heights[i,j];
				}
			}
			break;
		case Direction.NegativeX:
			for (int i=0; i<hmWidth; i++)
			{
				for (int j=PixelsToMove; j<hmHeight; j++)
				{
					heights[i,j-PixelsToMove] = heights[i,j];
				}
			}
			break;
		case Direction.PositiveZ:
			for (int i=hmWidth-1; i>=PixelsToMove; i--)
			{
				for (int j=hmHeight-1; j>=0; j--)
				{
					heights[i,j] = heights[i-PixelsToMove,j];
				}
			}
			break;
		case Direction.PositiveX:
			for (int i=hmWidth-1; i>=0; i--)
			{
				for (int j=hmHeight-1; j>=PixelsToMove; j--)
				{
					heights[i,j] = heights[i,j-PixelsToMove];
				}
			}
			break;
		}
		terr.terrainData.SetHeights(0,0,heights);
	}

	public enum Direction {NegativeZ, PositiveZ, NegativeX, PositiveX}
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Terrain))]
[ExecuteInEditMode()]
public class TerrainTool : MonoBehaviour {
	public int PixelsToMove = 50;
	public Direction directionToMove;

	Terrain terrain;

	void Awake()
	{
		terrain = gameObject.GetComponent<Terrain> ();
	}

	public void MoveTerrain()
	{
		int hmWidth = terrain.terrainData.heightmapWidth;
		int hmHeight = terrain.terrainData.heightmapHeight;
		int amWidth = terrain.terrainData.alphamapWidth;
		int amHeight = terrain.terrainData.alphamapHeight;

		float[,] heights = terrain.terrainData.GetHeights(0,0,hmWidth,hmHeight);
		float[,,] alphas = terrain.terrainData.GetAlphamaps (0, 0, amWidth, amHeight);

		switch(directionToMove)
		{
		case Direction.NegativeZ:
			MoveTerrainHeightmap_NegativeZ (heights);
			MoveTerrainAlphamap_NegativeZ (alphas);
			break;
		case Direction.NegativeX:
			MoveTerrainHeightmap_NegativeX(heights);
			MoveTerrainAlphamap_NegativeX (alphas);
			break;
		case Direction.PositiveZ:
			MoveTerrainHeightmap_PositiveZ(heights);
			MoveTerrainAlphamap_PositiveZ (alphas);
			break;
		case Direction.PositiveX:
			MoveTerrainHeightmap_PositiveX(heights);
			MoveTerrainAlphamap_PositiveX (alphas);
			break;
		}
		terrain.terrainData.SetHeights(0,0,heights);

	}
	//Heightmap
	void MoveTerrainHeightmap_NegativeZ(float[,] data)
	{
		int Height = data.GetLength(0);
		int Width = data.GetLength(1);

		for (int i=PixelsToMove; i<Width; i++)
		{
			for (int j=0; j<Height; j++)
			{
				data[i-PixelsToMove,j] = data[i,j];
			}
		}
	}
	void MoveTerrainHeightmap_NegativeX(float[,] data)
	{
		int Height = data.GetLength(0);
		int Width = data.GetLength(1);

		for (int i=0; i<Width; i++)
		{
			for (int j=PixelsToMove; j<Height; j++)
			{
				data[i,j-PixelsToMove] = data[i,j];
			}
		}
	}
	void MoveTerrainHeightmap_PositiveZ(float[,] data)
	{
		int Height = data.GetLength(0);
		int Width = data.GetLength(1);

		for (int i=Width-1; i>=PixelsToMove; i--)
		{
			for (int j=Height-1; j>=0; j--)
			{
				data[i,j] = data[i-PixelsToMove,j];
			}
		}
	}
	void MoveTerrainHeightmap_PositiveX(float[,] data)
	{
		int Height = data.GetLength(0);
		int Width = data.GetLength(1);

		for (int i=Width-1; i>=0; i--)
		{
			for (int j=Height-1; j>=PixelsToMove; j--)
			{
				data[i,j] = data[i,j-PixelsToMove];
			}
		}
	}
	//Alphamap
	void MoveTerrainAlphamap_NegativeZ(float[,,] data)
	{
		int Height = data.GetLength(0);
		int Width = data.GetLength(1);
		int Layers = data.GetLength(2);

		for (int i=PixelsToMove; i<Width; i++)
		{
			for (int j=0; j<Height; j++)
			{
				for (int layer = 0; layer < Layers; layer++) {
					data [i - PixelsToMove, j, layer] = data [i, j, layer];
				}
			}
		}
	}
	void MoveTerrainAlphamap_NegativeX(float[,,] data)
	{
		int Height = data.GetLength(0);
		int Width = data.GetLength(1);
		int Layers = data.GetLength(2);

		for (int i=0; i<Width; i++)
		{
			for (int j=PixelsToMove; j<Height; j++)
			{
				for (int layer = 0; layer < Layers; layer++) {
					data [i, j - PixelsToMove, layer] = data [i, j, layer];
				}
			}
		}
	}
	void MoveTerrainAlphamap_PositiveZ(float[,,] data)
	{
		int Height = data.GetLength(0);
		int Width = data.GetLength(1);
		int Layers = data.GetLength(2);

		for (int i=Width-1; i>=PixelsToMove; i--)
		{
			for (int j=Height-1; j>=0; j--)
			{
				for (int layer = 0; layer < Layers; layer++) {
					data [i, j, layer] = data [i - PixelsToMove, j, layer];
				}
			}
		}
	}
	void MoveTerrainAlphamap_PositiveX(float[,,] data)
	{
		int Height = data.GetLength(0);
		int Width = data.GetLength(1);
		int Layers = data.GetLength(2);

		for (int i=Width-1; i>=0; i--)
		{
			for (int j=Height-1; j>=PixelsToMove; j--)
			{
				for (int layer = 0; layer < Layers; layer++) {
					data [i, j, layer] = data [i, j - PixelsToMove, layer];
				}
			}
		}
	}
	//Detailmap
	void MoveTerrainDetailmap_NegativeZ(TerrainData data)
	{
		//for (int layer = 0; layer < data.detail
	}

	public enum Direction {NegativeZ, PositiveZ, NegativeX, PositiveX}
}

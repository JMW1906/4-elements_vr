﻿using UnityEngine;
using System.Collections;

public class Setup : MonoBehaviour {

	public GameObject[] ToBeSetup;

	void Awake()
	{
		for (int i = 0; i < ToBeSetup.Length; i++)
		{
			if (ToBeSetup[i] != null && !GameObject.Find(ToBeSetup[i].name))
			{
				GameObject obj = Instantiate(ToBeSetup[i]);
				obj.name = ToBeSetup[i].name;
			}
		}
		Destroy(gameObject);
	}
}

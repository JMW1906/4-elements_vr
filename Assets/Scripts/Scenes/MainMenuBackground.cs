﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuBackground : MonoBehaviour {

    public int sceneToAdd = 2;
    public bool forcekeepTimeScaleAt1 = true;

	void Awake ()
    {
        SceneManager.LoadScene(sceneToAdd, LoadSceneMode.Additive);
	}

    void Start()
    {
        CommonGameObjects.Player.SetActive(false);
    }

    void Update()
    {
        if (forcekeepTimeScaleAt1)
            Time.timeScale = 1;
    }
}

﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(TerrainTool))]
public class TerrainTool_Editor : Editor {

	private TerrainTool Target { 
		get { return (TerrainTool)target; } 
	}

	public int selectedTool = 0;
	public string[] toolbarStrings = { "Move Terrain", "Test2" };

	public override void OnInspectorGUI ()
	{
		selectedTool = GUILayout.Toolbar (selectedTool, toolbarStrings);
		GUILayout.Space (20);

		switch (selectedTool) {
		case 0:
			DrawTool_Verschieben ();
			break;
		case 1:
			GUILayout.Label ("1");
			break;
		default:
			Debug.LogError ("Terrain Tool: Invalid Tool Selected!");
			break;
		}
	}

	void DrawTool_Verschieben()
	{
		//Pixel to Move
		GUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField (new GUIContent ("Pixels to Move", "The Amount of Pixel the Terrain has to be moved"));
		Target.PixelsToMove = EditorGUILayout.IntField (Target.PixelsToMove);
		GUILayout.EndHorizontal ();

		//Direction
		GUILayout.BeginHorizontal ();
		EditorGUILayout.LabelField (new GUIContent ("Direction To Move To", "The Direction is only right if the Terrain isn't rotated"));
		Target.directionToMove = (TerrainTool.Direction)(EditorGUILayout.EnumPopup (Target.directionToMove));
		GUILayout.EndHorizontal ();

		//Button Move Terrain
		GUILayout.BeginHorizontal ();
		GUILayout.FlexibleSpace ();
		bool btn_MoveTerrain = GUILayout.Button (
			new GUIContent ("Move Terrain", "Moves the entire Terrain \nWARNING: Parts that get Moved so they are outside of the map won't exist anymore!"),
			GUILayout.ExpandWidth (false), 
			GUILayout.MaxWidth (180));
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal ();


		//Logik
		if (btn_MoveTerrain) {
			Target.MoveTerrain();
		}
	}
}

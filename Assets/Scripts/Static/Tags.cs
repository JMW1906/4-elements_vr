using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour {
	public const string Player = "Player";
	public const string GameController = "GameController";
	public const string Respawn = "Respawn";
	public const string Finish = "Finish";
	public const string MainCamera = "MainCamera";
	public const string UICamera = "UICamera";
	public const string UI = "UI";
	public const string AudioMixerControl = "AudioMixerControl";
	public const string CollectedObjects = "CollectedObjects";
    public const string EnergyBall = "Energy Ball";
}

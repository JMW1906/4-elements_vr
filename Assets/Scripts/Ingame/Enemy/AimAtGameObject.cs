﻿using UnityEngine;
using System.Collections;

public class AimAtGameObject : MonoBehaviour {

    public Transform Target;
    public float Speed;
    [Space()]
    public bool isShooting = false;
    public float maxAngleToTargetToShoot = 2;
    public float bulletSpeed = 10;
    [Space()]
    public bool onlyAimWhenTargetIsVisible = false;
    public int viewRange = 100;

    private ShootingObject m_shootingObject;
    private ShootingObject shootingObject{
        get{
            if (m_shootingObject == null && isShooting)
            {
                m_shootingObject = this.GetComponent<ShootingObject>();
                if (m_shootingObject == null)
                    Debug.LogError("There is no \"Shooting Object\" Component on this Object");
            }
            return m_shootingObject;
        }
        set{
            m_shootingObject = value;
        }
    }

	void Awake () {
        shootingObject = this.GetComponent<ShootingObject>();
	}
	
    void FixedUpdate () {
        Vector3 targetDirection = Target.position - transform.position;
        if (onlyAimWhenTargetIsVisible)
        {
            Ray ray = new Ray(this.transform.position, targetDirection);
            RaycastHit hitInfo;
            if (Physics.Raycast(ray, out hitInfo, viewRange))
            {
                if (hitInfo.collider.tag != Tags.Player)
                    return;
            }
            else
                return;
        }
        Quaternion rotation = Quaternion.LookRotation(targetDirection);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * Speed);

        if (isShooting && DoesAimAtTarget(targetDirection, transform.forward))
        {
            shootingObject.BulletMovement = transform.forward.normalized * bulletSpeed;
            shootingObject.Shoot();
        }
	}

    bool DoesAimAtTarget(Vector3 targetDirection, Vector3 direction)
    {
        if (Vector3.Angle(targetDirection, direction) < maxAngleToTargetToShoot)
            return true;
        return false;
    }
}

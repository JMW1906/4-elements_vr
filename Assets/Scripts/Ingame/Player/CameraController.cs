﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl))]
public class CameraController : MonoBehaviour {

	#region Fields/Properties
	[Header("GameObjects for Camera Moving")]
    public GameObject FirstPersonCameraPos;
    public GameObject ThirdPersonCameraRotPoint;
	public GameObject ThirdPersonCameraPos;
	public GameObject MovingCamera;

	[Header("Camera Movement Settings")]
	//Mouse Sensibility
	public Vector2 MouseSensibility;
	//Max Camera Rotation
	public MinMax_Rotation Rotation_FirstPerson_Y;
	public MinMax_Rotation Rotation_ThirdPerson_Y;
	public float ThirdPersonCamera_MaxDistanceFromPlayer = 1.9f;

	[Space(), Tooltip("GameObjects die im First Person nicht sichtbar sind, nur im Third Person")]
	public SkinnedMeshRenderer[] VisiblePlayerParts;

	[Space(), Header("Aktuelle Positionen")]
	public float CameraAngleVertical = 0;
	public float CameraAngleHorizontal = 0; //Nur Third person
	public bool IsFirstPerson = false;
	#endregion

	void Update () {
		if (Input.GetButtonDown(InputNames.SwitchPerspective) && Time.timeScale != 0) {
			ChangeCameraView();
		}
	}
	void FixedUpdate(){
		MouseMovement();
	}

	void MouseMovement() {
		float AxisY = Input.GetAxis(InputNames.MouseY) * MouseSensibility.y;
		float AxisX = Input.GetAxis(InputNames.MouseX) * MouseSensibility.x;
		
		CameraAngleVertical += AxisY;
        if (IsFirstPerson){
			CameraAngleVertical = Mathf.Clamp(CameraAngleVertical, Rotation_FirstPerson_Y.Min, Rotation_FirstPerson_Y.Max);
			FirstPersonCameraPos.transform.localRotation = Quaternion.AngleAxis(CameraAngleVertical, -Vector3.right);
			transform.Rotate(0, AxisX, 0);
		}
		else{
			CameraAngleVertical = 
				Mathf.Clamp(CameraAngleVertical, Rotation_ThirdPerson_Y.Min, Rotation_ThirdPerson_Y.Max);
			ThirdPersonCameraRotPoint.transform.localRotation = 
				Quaternion.AngleAxis(CameraAngleVertical, -Vector3.right);
	//Bringt Kamera dazu von Wänden abzuprallen
			Ray ray = new Ray(ThirdPersonCameraRotPoint.transform.position, -ThirdPersonCameraRotPoint.transform.forward);
			RaycastHit rayInfo;
			Physics.Raycast(ray, out rayInfo, ThirdPersonCamera_MaxDistanceFromPlayer);
			ThirdPersonCameraPos.transform.localPosition = new Vector3(0,0,
				-((rayInfo.distance != 0)?
			    	Mathf.Min(ThirdPersonCamera_MaxDistanceFromPlayer, rayInfo.distance):
					ThirdPersonCamera_MaxDistanceFromPlayer));
		}
	}
	
	void ChangeCameraView() {
		if (IsFirstPerson) {
			MovingCamera.transform.SetParent(ThirdPersonCameraPos.transform, false);
			CameraAngleHorizontal = 0;
			for (int i = 0; i < VisiblePlayerParts.Length; i++){ //Versteckt das Spieler Modell
				VisiblePlayerParts[i].enabled = true;
			}
		}
		else {
			MovingCamera.transform.SetParent(FirstPersonCameraPos.transform, false);
			for (int i = 0; i < VisiblePlayerParts.Length; i++){ //zeigt Spieler Modell wieder an
				VisiblePlayerParts[i].enabled = false;
            }
		}
		IsFirstPerson = !IsFirstPerson;
	}
}













﻿using UnityEngine;
using System.Collections;

public class Player_SoundManager : MonoBehaviour {
	
	private AudioSource FootstepAudioSrc;

	void Start () {
		FootstepAudioSrc = gameObject.GetComponent<AudioSource>();
	}
	
	void Update () {
		if (Time.timeScale != 0){
			if (Input.GetAxisRaw(InputNames.Vertical) > 0) 
			{
				if (!FootstepAudioSrc.isPlaying)
					FootstepAudioSrc.Play();
			}
			else
			{
				FootstepAudioSrc.Stop();
			}
		}
		else{
			FootstepAudioSrc.Stop();
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;


public class Health : MonoBehaviour {

	public int health;
	public bool showRespawnScreen = false;

	[SerializeField]
	OnDeathEvent onDeath = new OnDeathEvent(); 

	public bool IsDead{
		get{ 
			if (health <= 0) {
				Invoke_OnDeath ();
				return true;
			}
			else
				return false;
		}
	}

	void Invoke_OnDeath()
	{
		Debug.Log (gameObject +"Died");
		if (showRespawnScreen)
			MenuManager.Instance.ShowMenu (MenuManager.Instance.RespawnMenu);
		if (onDeath != null)
			onDeath.Invoke ();
	}

	public void DealDamage(int damageAmount)
	{
		if (damageAmount == -1) {
			health = 0;
			Invoke_OnDeath ();
		}
		else {
			health -= damageAmount;
			if (IsDead) {
				health = 0;
			}
		}
	}

	[System.Serializable]
	public class OnDeathEvent : UnityEvent {}
}

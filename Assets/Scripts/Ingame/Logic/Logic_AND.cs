﻿using UnityEngine;
using System.Collections;

public class Logic_AND : MonoBehaviour {

	[SerializeField]
	public UnityEngine.Events.UnityEvent onAllOn;


	private bool[] state = new bool[2];


	public void EnableInput(int index)
	{
		state [index] = true;
		Check ();
	}
	public void DisableInput(int index)
	{
		state [index] = false;
		Check ();
	}

	private void Check()
	{
		int count = 0;
		for (int i = 0; i < state.Length; i++) {
			if (state [i])
				count++;
		}
		if (count == state.Length)
		{
			onAllOn.Invoke ();
		}
	}
}

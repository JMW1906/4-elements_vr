﻿using UnityEngine;
using System.Collections;

public class ShootingObject : MonoBehaviour {

	public GameObject bullet;
	public Transform bulletSpawnPoint;
	[Tooltip("Zeit in Sekunden")]
	public float WaitTimeBetweenShots = 1;
	public Vector3 BulletMovement;
    public bool playAudioClipOnShoot;

	private float LastShot;
    private AudioSource audioSrc;

    void Awake()
    {
        audioSrc = this.GetComponent<AudioSource>();
    }

	public void Shoot()
	{
		if (LastShot + WaitTimeBetweenShots > Time.time)
			return;

		Debug.Log (gameObject+" Schießt");
		if (bulletSpawnPoint == null)
			bulletSpawnPoint = transform;
		GameObject b = (GameObject)Instantiate(bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
		b.transform.localScale = bulletSpawnPoint.lossyScale;
		b.GetComponent<AutoMove>().movement = BulletMovement;

        if (playAudioClipOnShoot)
            audioSrc.Play();

		LastShot = Time.time;
	}
}

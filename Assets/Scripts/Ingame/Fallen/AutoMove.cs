using UnityEngine;
using System.Collections;

public class AutoMove : MonoBehaviour
{
	public Vector3 movement;

	void FixedUpdate()
	{
		transform.position += movement * Time.deltaTime;
	}
}


﻿using UnityEngine;
using System.Collections;

public class Flammenfalle : MonoBehaviour {

	[Tooltip("Zeit in Sekunden")]
	public float TimeOn = 1;

	public ParticleSystem Flames;

	ParticleSystem[] Flame_Childs{
		get{
			return Flames.GetComponentsInChildren<ParticleSystem> ();
		}
	}


	public void Trigger()
	{
		Debug.Log (gameObject+" Activated");
		if (!Flames.isPlaying) {
			SetupParticleSystem (true);
			Flames.gameObject.SetActive (false);
			Flames.gameObject.SetActive (true);
		}

		Invoke ("unTrigger", TimeOn);
	}

	private void unTrigger()
	{
		Debug.Log (gameObject+" Deactivated");
		SetupParticleSystem (false);
	}

	private void SetupParticleSystem(bool enabled)
	{
		Flames.loop = enabled;
		ParticleSystem[] childs = Flame_Childs;
		for (int i = 0; i < childs.Length; i++) {
			childs [i].loop = enabled;
		}
		Flames.GetComponent<Collider> ().enabled = enabled;
	}
}

﻿using UnityEngine;
using System.Collections;

public class DamageDealer : MonoBehaviour {

	public bool dealDamageOverTime = false;
	[Tooltip("Set it to -1 to kill instant")]
	public int damageAmount = 10;
	[Tooltip("Leave Empty for Allowing all Tags to get Damaged")]
	public string RequiredTag = "";


	void OnCollisionEnter(Collision other)
	{
		Damage (other.gameObject);
	}

	void OnCollisionStay(Collision other)
	{
		if (dealDamageOverTime) {
			Damage (other.gameObject);
		}
	}
	void OnTriggerEnter(Collider other)
	{
		Damage (other.gameObject);
	}

	void OnTriggerStay(Collider other)
	{
		if (dealDamageOverTime) {
			Damage (other.gameObject);
		}
	}


	void Damage(GameObject other)
	{
		try
		{
			if (RequiredTag == "" || other.tag == RequiredTag)
			{
				other.GetComponent<Health> ().DealDamage (damageAmount);
				Debug.LogFormat ("{0} fügt {1} {2} Schaden zu", gameObject, other, damageAmount);
			}
		}
		catch{
			
		}
	}
}

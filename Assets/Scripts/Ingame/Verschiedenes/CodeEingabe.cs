﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.UI;

public class CodeEingabe : MonoBehaviour {

    public int selection;
    private CodeEingabeÜberpruefung ueberpruefung;
    private Image image;

    void Start()
    {
        ueberpruefung = GetComponentInParent<CodeEingabeÜberpruefung>();
        image = this.GetComponent<Image>();
        image.sprite = ueberpruefung.auswahlmoeglichkeiten[selection];
    }

    public void NextSelection()
    {
        selection++;
        if (selection >= ueberpruefung.AnzAuswahlmoeglichkeiten)
            selection = 0;
        SelectionUpdated();
    }
    public void LastSelection()
    {
        if (selection == 0)
            selection = ueberpruefung.AnzAuswahlmoeglichkeiten;
        selection--;
        SelectionUpdated();
    }

    void SelectionUpdated()
    {
        image.sprite = ueberpruefung.auswahlmoeglichkeiten[selection];
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class CodeEingabeÜberpruefung : MonoBehaviour {

    public Transform auswahlschalter_parent;
    public GameObject auswahlschalter_prefab;

    public int[] correctSelections;
    public Sprite[] auswahlmoeglichkeiten;

    public UnityEvent OnWrongSelection;
    public UnityEvent OnRightSelection;


    private CodeEingabe[] codeEingaben;


    public int AnzAuswahlmoeglichkeiten{
        get{ return auswahlmoeglichkeiten.Length; }
    }


    void Awake()
    {
        while (auswahlschalter_parent.childCount < correctSelections.Length)
        {
            Transform obj = ((GameObject)GameObject.Instantiate(
                auswahlschalter_prefab, 
                auswahlschalter_parent.GetChild(0).position, 
                auswahlschalter_parent.GetChild(0).rotation)).transform;
            obj.SetParent(auswahlschalter_parent, true);
        }

        codeEingaben = new CodeEingabe[auswahlschalter_parent.childCount];
        for (int i = 0; i < codeEingaben.Length; i++)
        {
            codeEingaben[i] = auswahlschalter_parent.GetChild(i).GetComponent<CodeEingabe>();
        }
    }

    public void CheckIfCorrect()
    {
        for (int i = 0; i < correctSelections.Length; i++)
        {
            if (codeEingaben[i].selection != correctSelections[i])
            {
                OnWrongSelection.Invoke();
                return;
            }
        }
        OnRightSelection.Invoke();
    }
}

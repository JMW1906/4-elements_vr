﻿using UnityEngine;
using System.Collections;

public class FPSDisplay : MonoBehaviour {

	Rect position = new Rect(100, 100, 100, 100);
	
	void OnGUI () {
		float fps = 1 / Time.deltaTime;
		GUI.Label (position, "FPS: " + fps);
	}
}

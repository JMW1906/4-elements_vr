﻿using UnityEngine;
using System.Collections;

public class OpenNextLevel : MonoBehaviour {

    SceneController sc;

    void Awake()
    {
        sc = CommonGameObjects.GameController.GetComponent<SceneController>();
    }

    public void _OpenNextLevel()
    {
        sc.LoadLevel(sc.actuelleScene + 1);
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;


[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(FollowTargetWithRotate))]
public class VerschiebaresObjekt : MonoBehaviour {

//Public Variables
	public KeyCode KeyToHold;


//Private Variables
	bool isInRange = false;
    Rigidbody rigidbody;
    FollowTargetWithRotate followTarget;


//Called Methods
    void Awake()
    {
        rigidbody = gameObject.GetComponent<Rigidbody>();
        followTarget = GetComponent<FollowTargetWithRotate>();
    }
	void Start(){
        this.transform.SetParent(null);
        followTarget.target = CommonGameObjects.Player.transform.GetChild(0);
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == Tags.Player)
			isInRange = true;
	}
	void OnTriggerExit(Collider other)
	{
		if (other.tag == Tags.Player)
			isInRange = false;
	}
	void Update()
	{
        if (isInRange || followTarget.enabled)
            KeyCheck();
	}


//Methods
	void KeyCheck()
	{
        if (Input.GetKeyUp (KeyToHold)) {
            rigidbody.velocity = Vector3.zero;
            followTarget.enabled = false;
            rigidbody.useGravity = true;
		}
        else if (Input.GetKeyDown (KeyToHold)) {
            followTarget.enabled = true;
            rigidbody.useGravity = false;
		}
	}
}

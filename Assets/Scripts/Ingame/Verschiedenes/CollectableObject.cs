﻿using UnityEngine;
using System.Collections;

public class CollectableObject : MonoBehaviour {

	public Sprite SpriteWhenInInventory;
	public WhatToDo WhatToDoAfterCollected;

	public void Collect()
	{
		if (WhatToDoAfterCollected != WhatToDo.Destroy)
		{
			CollectedObjectManager.Instance.AddObject(SpriteWhenInInventory, gameObject);
			if (WhatToDoAfterCollected == WhatToDo.Hide)
				gameObject.SetActive(false);
		}
		else
		{
			CollectedObjectManager.Instance.AddObject(SpriteWhenInInventory, null);
			Destroy(gameObject);
		}
	}

	public enum WhatToDo {Hide, Destroy, Keep}
}

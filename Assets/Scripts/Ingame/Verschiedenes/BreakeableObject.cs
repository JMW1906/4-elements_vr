﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class BreakeableObject : MonoBehaviour {

    public float MaxVelocityChange;
    public bool OnlyAddChildObjects;
    public GameObject BrokenObject;

    Rigidbody rb;
    float oldVelocity;


	void Start () {
        rb = GetComponent<Rigidbody>();
        oldVelocity = GetVelocity();
	}
	
	void Update () {
        float newVelocity = GetVelocity();
        if (Difference(oldVelocity, newVelocity) > MaxVelocityChange)
        {
            Break();
        }
        oldVelocity = newVelocity;
	}

    float GetVelocity(){
        return rb.velocity.magnitude;
    }
    float Difference(float f1, float f2)
    {
        return (f1 < f2) ? (f2-f1) : (f1-f2);
    }

    public void Break()
    {
        Transform brokenObject = GameObject.Instantiate<GameObject>(BrokenObject).transform;
        brokenObject.position = this.transform.position;
        brokenObject.rotation = this.transform.rotation;
        brokenObject.localScale = this.transform.lossyScale;

        for(int i = 0; i < brokenObject.childCount; i++)
        {
            Rigidbody childRb = brokenObject.GetChild(i).GetComponent<Rigidbody>();
            //childRb.velocity = rb.velocity;
        }

       

        GameObject.Destroy(this.gameObject);
    }
}

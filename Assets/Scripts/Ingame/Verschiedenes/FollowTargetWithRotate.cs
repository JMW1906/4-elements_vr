﻿using UnityEngine;
using System.Collections;

public class FollowTargetWithRotate : MonoBehaviour {

    public Transform target;
    public Vector3 offset = new Vector3(0f, 7.5f, 0f);


    private void LateUpdate()
    {
        transform.position = target.position + offset;
        transform.rotation = target.rotation;
    }
}

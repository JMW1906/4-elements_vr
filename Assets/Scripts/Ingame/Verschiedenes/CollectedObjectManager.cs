﻿using UnityEngine;
using System.Collections;

public class CollectedObjectManager : MonoBehaviour {

	public static CollectedObjectManager Instance;

	public GameObject CollectedObject_Prefab;

	void Awake()
	{
		Debug.Log("Instance");
		Instance = this;
	}

	public void AddObject(Sprite sprite, GameObject objectToPlace)
	{
		GameObject obj = GameObject.Instantiate(CollectedObject_Prefab);
		obj.transform.SetParent(transform, false);
		obj.GetComponent<UnityEngine.UI.Image>().sprite = sprite;
		obj.GetComponent<CollectedObject>().objectToPlace = objectToPlace;
        obj.name = objectToPlace.name;
	}

    public CollectedObject TestWithName(string name)
    {
        return transform.FindChild(name).GetComponent<CollectedObject>();
    }

    public void RemoveObject(CollectedObject obj)
    {
        GameObject.Destroy(obj.gameObject);
    }
}

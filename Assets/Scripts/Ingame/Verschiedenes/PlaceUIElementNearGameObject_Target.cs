﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (Collider))]
public class PlaceUIElementNearGameObject_Target : Trigger_Check {

	public string TextToDisplay;

	void OnTriggerEnter(Collider other)
	{
        if (IsValidGameObject(other) &&
            PlaceUIElementNearGameObject.Instance.Target == null)
        {
			PlaceUIElementNearGameObject.Instance.SetTarget (this.gameObject, TextToDisplay);
        }
	}
	void OnTriggerExit(Collider other)
    {
        if (IsValidGameObject(other) &&
            PlaceUIElementNearGameObject.Instance.Target == this.gameObject)
        {
			PlaceUIElementNearGameObject.Instance.SetTarget (null);
        }
	}
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class Animator_SetParameter : MonoBehaviour {
	Animator anim;

	public string Animation_Parameter = "IsTriggered";

	void Start()
	{
		anim = gameObject.GetComponent<Animator>();
	}

	public void EnableParameter(bool enabled)
	{
		anim.SetBool(Animation_Parameter, enabled);
	}

	public void TriggerParameter () {
		try{
			anim.SetBool(Animation_Parameter, !anim.GetBool(Animation_Parameter));
			Debug.LogFormat ("{0} | Parameter '{1}' wurde auf geaendert", this, Animation_Parameter);
		}
		catch{
			Debug.LogError ("Parameter is not of the Type bool!");
		}
	}
	public void TriggerParameter_Later(float delay)
	{
		Invoke("TriggerParameter",delay); 
	}
}

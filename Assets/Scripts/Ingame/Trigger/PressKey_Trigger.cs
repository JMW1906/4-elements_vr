﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class PressKey_Trigger : Trigger_Check {

//Public Variables
	public KeyCode Key;

	[SerializeField]
	protected UnityEvent onKeyDown = new UnityEvent(); 
	[SerializeField, UnityEngine.Serialization.FormerlySerializedAs("onTrigger")]
	protected UnityEvent onKeyUp = new UnityEvent(); 


//Priavte Variables
	bool isInRange = false;


//Properties
	public UnityEvent OnKeyDown
	{
		get
		{
			return this.onKeyDown;
		}
		set
		{
			this.onKeyDown = value;
		}
	}
	public UnityEvent OnKeyUp
	{
		get
		{
			return this.onKeyUp;
		}
		set
		{
			this.onKeyUp = value;
		}
	}


//Called Methods
	void OnTriggerEnter(Collider other)
	{
		if (IsValidGameObject(other))
			isInRange = true;
	}
	void OnTriggerExit(Collider other)
	{
		if (IsValidGameObject(other))
			isInRange = false;
	}
	void Update()
	{
		if (isInRange)
			KeyCheck ();
	}


//Methods
	void KeyCheck()
	{
		if (Input.GetKeyUp (Key)) {
			InvokeOnKeyUp ();
		} else if (Input.GetKeyDown (Key)) {
			InvokeOnKeyDown ();
		}
	}
	protected void InvokeOnKeyDown()
	{
		OnKeyDown.Invoke();
		Debug.LogFormat ("{0} | Taste {1} wurde gedrückt", this, Key);
	}
	protected void InvokeOnKeyUp()
	{
		OnKeyUp.Invoke();
		Debug.LogFormat ("{0} | Taste {1} wurde losgelassen", this, Key);
	}
}

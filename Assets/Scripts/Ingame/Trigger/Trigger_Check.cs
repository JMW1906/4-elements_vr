﻿using UnityEngine;
using System.Collections;

public class Trigger_Check : MonoBehaviour {
	
	#region Fields
	public float triggerWaitTime = 0f;

	[SerializeField, Tooltip("Leave Empty for ignoring the tag!")]
	protected string requiredTag;
	#endregion


	protected bool IsValidGameObject(Collider other)
	{
		return (requiredTag == "" || other == null || other.tag == requiredTag);
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Trigger : Trigger_Base {

	[SerializeField]
	enum_TriggerType triggerType;

	public enum_TriggerType TriggerType {
		get {
			return triggerType;
		}
		set {
			triggerType = value;
		}
	}

	#region TriggerMethods
	public void OnCollisionEnter(Collision other)
	{
		if (triggerType == enum_TriggerType.OnCollisionEnter)
			InvokeOnTrigger (other.collider);
	}
	public void OnCollisionExit(Collision other)
	{
		if (triggerType == enum_TriggerType.OnCollisionExit)
			InvokeOnTrigger (other.collider);
	}
	public void OnCollisionStay(Collision other)
	{
		if (triggerType == enum_TriggerType.OnCollisionStay)
			InvokeOnTrigger (other.collider);
	}
	public void OnTriggerEnter(Collider other)
	{
		if (triggerType == enum_TriggerType.OnTriggerEnter)
			InvokeOnTrigger (other);
	}
	public void OnTriggerExit(Collider other)
	{
		if (triggerType == enum_TriggerType.OnTriggerExit)
			InvokeOnTrigger(other);
	}
	public void OnTriggerStay(Collider other)
	{
		if (triggerType == enum_TriggerType.OnTriggerStay)
			InvokeOnTrigger(other);
	}
	public void OnLevelWasLoaded()
	{
		if (triggerType == enum_TriggerType.OnLevelWasLoaded)
			InvokeOnTrigger();
	}
	#endregion

	#region Nested
	public enum enum_TriggerType {OnTriggerEnter, OnTriggerExit, OnTriggerStay, OnLevelWasLoaded, OnCollisionEnter, OnCollisionExit, OnCollisionStay, AskForKeyPress}
	#endregion
}

﻿using UnityEngine;
using System.Collections;

public class Schluesselloch : Trigger_Base {

    public string NameOfKey = "Schlüssel";

    public void InsertKey()
    {
        CollectedObject obj = CollectedObjectManager.Instance.TestWithName(NameOfKey);
        if (obj != null)
        {
            CollectedObjectManager.Instance.RemoveObject(obj);
            InvokeOnTrigger();
        }
    }
}

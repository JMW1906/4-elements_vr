﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class Trigger_Base : Trigger_Check {

	#region Fields
	[SerializeField]
	protected OnTriggerEvent onTrigger = new OnTriggerEvent(); 
	#endregion

	#region Properties
	public OnTriggerEvent OnTrigger
	{
		get
		{
			return this.onTrigger;
		}
		set
		{
			this.onTrigger = value;
		}
	}
	#endregion

	#region Methods
	protected void InvokeOnTrigger(Collider other)
	{
		if (IsValidGameObject(other))
		{
			if (triggerWaitTime == 0f)
				InvokeOnTrigger();
			else
				Invoke("InvokeOnTrigger", triggerWaitTime);
		}
	}
	protected void InvokeOnTrigger()
	{
		onTrigger.Invoke();
		Debug.LogFormat ("{0} | onTrigger-Event wurde ausgeloeßt", this);
	}
	#endregion


	#region Nested
	[System.Serializable]
	public class OnTriggerEvent : UnityEvent {}
	#endregion
}

﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UnityEngine.UI.Slider))]
public class AudioVol_Slider : MonoBehaviour {

	public enum AudioMixerGroup {MusikVol, SfxVol}

	public Menu menu;
	public AudioMixerGroup audioMixerGroupToChange;

	private UnityEngine.UI.Slider slider;
	private float vol_old = 0;



	void Awake()
	{
		slider = gameObject.GetComponent<UnityEngine.UI.Slider>();
	}

	void Start()
	{
		menu.onOpen.AddListener(OnMenuOpen);
	}

	public void SetAudioMixerVol()
	{
		if (audioMixerGroupToChange == AudioMixerGroup.MusikVol)
			SetAudioMixerMusikVol();
		else
			SetAudioMixerSfxVol();
	}

	private void SetAudioMixerMusikVol(){
		float vol = slider.value;
		if (vol <= -21)
			vol = -80;
		AudioMixerControl.Instance.SetMusikVolumeLvl(vol);
	}
	private void SetAudioMixerSfxVol(){
		float vol = slider.value;
		if (vol <= -21)
			vol = -80;
		AudioMixerControl.Instance.SetSoundEffectVolumeLvl(vol);
	}

	public void OnMenuOpen()
	{
		if (audioMixerGroupToChange == AudioMixerGroup.MusikVol)
			AudioMixerControl.Instance.GetMusikVolumeLvl(out vol_old);
		else
			AudioMixerControl.Instance.GetSoundEffectVolumeLvl(out vol_old);
		slider.value = vol_old;
	}

	public void OnButtonCancel()
	{
		if (audioMixerGroupToChange == AudioMixerGroup.MusikVol)
			AudioMixerControl.Instance.SetMusikVolumeLvl(vol_old);
		else
			AudioMixerControl.Instance.SetSoundEffectVolumeLvl(vol_old);
	}
}

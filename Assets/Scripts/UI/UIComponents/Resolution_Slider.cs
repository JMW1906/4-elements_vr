﻿using UnityEngine;
using System.Collections;

public class Resolution_Slider : MonoBehaviour {

	#region Fields
	//0: 16:9
	//1: 4:3
	//2: 16:10
	private static int[,] Resolutions = {
		{640,480, 1},
		{720,480, 2},
		{720,576, 1},
		{800,600, 1},
		{1024,768, 1},
		{1152,864, 1},
		{1280,720, 0},
		{1280,768, 2},
		{1280,960, 1},
		{1280,1024, 1},
		{1360,768, 0},
		{1440,900, 2},
		{1600,900, 0},
		{1600,1200, 1},
		{1680,1050, 2},
		{1920,1080, 0}
	};
	private static string[] Verhaeltniss = {
		"16:9", "4:3", "16:10"
	};

	private int selectedIndex;

	private string m_ResolutionLabel_Text = "Auflösung\n";
	#endregion
	#region Properties
	public string ResolutionLabel_Text
	{
		get{ return m_ResolutionLabel_Text; }
		set
		{
			m_ResolutionLabel_Text = value; 
			onSliderResolution_valueChanged(Slider.value);
		}
	}
	public UnityEngine.UI.Text Resolution_Label;
	public UnityEngine.UI.Slider Slider;
	public UnityEngine.UI.Toggle Fullscreen_Toggle;
	#endregion


	#region Methods
	void Start(){
		ResetResolutionAndFullscreenElement();

		Slider.onValueChanged.AddListener(onSliderResolution_valueChanged);

		onSliderResolution_valueChanged(Slider.value);
	}
	int GetResolutionIndex()
	{
		return GetResolutionIndex(Screen.width, Screen.height);
	}
	int GetResolutionIndex(int width, int height)
	{
		for (int i = 0; i < Resolutions.GetLength(0); i++)
		{
			if (width == Resolutions[i, 0] &&
			    height == Resolutions[i, 1])
			{
				return i;
			}
		}
		if (!Application.isEditor)
			Debug.LogWarning("Unknown Resolution: "+width.ToString()+"*"+height.ToString()+"!");
		return -1;
	}

	public void onSliderResolution_valueChanged(float value){
		selectedIndex = (int)value;
		Resolution_Label.text = ResolutionLabel_Text + Resolutions[selectedIndex, 0] + "x" + Resolutions[selectedIndex, 1] + " (" + Verhaeltniss[Resolutions[selectedIndex, 2]] + ")";
	}

	
	public void ChangeResolution(){
		ChangeResolution(Fullscreen_Toggle.isOn);
	}
	public void ChangeResolution(bool isFullscreen){
		Screen.SetResolution(Resolutions[selectedIndex, 0], Resolutions[selectedIndex, 1], isFullscreen);
	}
	public void ResetResolutionAndFullscreenElement()
	{
		Slider.value = GetResolutionIndex();
		if (Fullscreen_Toggle != null)
			Fullscreen_Toggle.isOn = Screen.fullScreen;
	}
	#endregion
}

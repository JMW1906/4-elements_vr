﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour 
{
	#region Fields
	[SerializeField]
	private Menu.MenuStateChangedEvent m_OnOpen = new Menu.MenuStateChangedEvent();
	[SerializeField]
	private Menu.MenuStateChangedEvent m_OnClose = new Menu.MenuStateChangedEvent();
	[SerializeField]
	private Menu.EscapePressedEvent m_OnEscapePressed = new Menu.EscapePressedEvent();
	private Menu m_CalledFrom;
	private Animator _animator;
	private CanvasGroup _canvasGroup;
	[SerializeField]
	private float m_animation_Speed = 1;
	#endregion
	#region Properties
	public Menu.MenuStateChangedEvent onOpen
	{
		get
		{
			return this.m_OnOpen;
		}
		set
		{
			this.m_OnOpen = value;
		}
	}
	public Menu.MenuStateChangedEvent onClose
	{
		get
		{
			return this.m_OnClose;
		}
		set
		{
			this.m_OnClose = value;
		}
	}
	public Menu.EscapePressedEvent OnEscapePressed
	{
		get
		{
			return this.m_OnEscapePressed;
		}
		set
        {
			this.m_OnEscapePressed = value;
        }
    }
	public bool IsOpen
	{
		get { return _animator.GetBool("IsOpen"); }
		set 
		{
			_animator.speed = Animation_Speed;
			_animator.SetBool("IsOpen", value); 
			if (value)
				this.m_OnOpen.Invoke();
			else
				this.m_OnClose.Invoke();
		}
	}
	public Menu CalledFrom
	{
		get{ return m_CalledFrom; }
		set{ m_CalledFrom = value; }
	}
	public float Animation_Speed
	{
		get{ return m_animation_Speed; }
		set{ m_animation_Speed = value; }
	}
	public bool PauseGameWhileOpen = true;
	#endregion

	#region Methods
	public void Awake()
	{
		_animator = GetComponent<Animator>();
		_canvasGroup = GetComponent<CanvasGroup>();

		var rect = GetComponent<RectTransform>();
		rect.localPosition = Vector3.zero;
	}

	public void SetActive(bool enabled)
	{
		if (enabled)
		{
			_canvasGroup.blocksRaycasts = _canvasGroup.interactable = true;
		}
		else
		{
			_canvasGroup.blocksRaycasts = _canvasGroup.interactable = false;
		}
	}

	#endregion


	
	#region Nestet
	[System.Serializable]
	public class MenuStateChangedEvent : UnityEngine.Events.UnityEvent
    {
	}
	[System.Serializable]
	public class EscapePressedEvent : UnityEngine.Events.UnityEvent
    {
    }
    #endregion
}

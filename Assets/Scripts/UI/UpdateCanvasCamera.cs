﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Canvas))]
public class UpdateCanvasCamera : MonoBehaviour {

	Canvas canvas_Component;
	void Awake()
	{
		canvas_Component = gameObject.GetComponent<Canvas>();
	}

	void Start()
	{
		SetNewCanvas_RenderCamera();
	}

	void OnLevelWasLoaded(int level)
	{
		SetNewCanvas_RenderCamera();
	}
	
	private void SetNewCanvas_RenderCamera()
	{
		GameObject UICam = GameObject.FindWithTag(Tags.UICamera);
		if (UICam != null)
		{
			canvas_Component.worldCamera = UICam.GetComponent<Camera>();
			return;
		}
		canvas_Component.worldCamera = Camera.main;
	}
}

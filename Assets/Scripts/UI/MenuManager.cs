﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource), typeof(CanvasGroup), typeof(Animator)), RequireComponent(typeof(Canvas))]
public class MenuManager : MonoBehaviour {

	#region Fields
	private AudioSource audioSrc_Component;
	private Animator _animator;
	#endregion

	#region Properties
	public Menu CurrentMenu;
	public AudioSetting ButtonClickSound;
	public Menu ShowWhenEscapePressed;
	public static MenuManager Instance;


	public Menu MainMenu;
	public Menu RespawnMenu;

	
	public bool IsVisible
	{
		get { return _animator.GetBool("IsVisible"); }
		set 
		{
			_animator.SetBool("IsVisible", value);
		}
	}
	#endregion

	#region Methods
	void Awake()
	{
		Instance = this;
		audioSrc_Component = gameObject.GetComponent<AudioSource>();
		_animator = gameObject.GetComponent<Animator>();
	}
	void Start()
	{
		ShowMenu(CurrentMenu);
	}

	void Update()
	{
		if (Input.GetButtonDown(InputNames.Cancel))
		{
			if (CurrentMenu != null)
				CurrentMenu.OnEscapePressed.Invoke();
			else
				ShowMenu(ShowWhenEscapePressed);
		}
	}

	public void ShowMenu(Menu menu)
	{
		ShowMenu(menu, true);
	}
	public void ShowMenu(Menu menu, bool SetCurrentMenuAsCalledFrom)
	{
		Debug.Log("Show Menu "+menu);
		if (menu == null)
		{
			Time.timeScale = 1f;
			return;
		}
		else if (menu.PauseGameWhileOpen)
			Time.timeScale = 0f;
		else
			Time.timeScale = 1f;
		_animator.speed = 1f;
		IsVisible = true;
		if (CurrentMenu != null)
		{
			CurrentMenu.IsOpen = false;
			if (SetCurrentMenuAsCalledFrom)
            	menu.CalledFrom = CurrentMenu;
        }
        CurrentMenu = menu;
        CurrentMenu.IsOpen = true;
    }
	public void ShowLastMenu()
	{
		Debug.Log("Show Last Menu");
		if (CurrentMenu != null)
			ShowMenu(CurrentMenu.CalledFrom, false);
	}
	public void ShowMenuAsPopup(Menu menu)
	{
		Debug.Log("Show Menu as Popup "+menu);
		if (menu == null)
		{
			Time.timeScale = 1f;
			return;
		}
		else if (menu.PauseGameWhileOpen)
			Time.timeScale = 0f;
		else
			Time.timeScale = 1f;
		_animator.speed = 1f;
		IsVisible = true;
		if (CurrentMenu != null)
		{
			menu.CalledFrom = CurrentMenu;
		}
		CurrentMenu = menu;
		CurrentMenu.IsOpen = true;
	}

	public void HideMenu()
	{
		Time.timeScale = 1f;
		_animator.speed = 1f;
		IsVisible = false;
		if (CurrentMenu != null)
			CurrentMenu.IsOpen = false;
		CurrentMenu = null;
    }
	public void HideMenuInstantly()
	{
		Time.timeScale = 1f;
		_animator.speed = 10f;
		IsVisible = false;
		if (CurrentMenu != null)
			CurrentMenu.IsOpen = false;
		CurrentMenu = null;
	}
	public void CloseAllMenus()
	{
		while(CurrentMenu != null)
		{
			Debug.Log(CurrentMenu + " closed");
			CurrentMenu.IsOpen = false;
			CurrentMenu = CurrentMenu.CalledFrom;
		}
		Debug.Log("Closing Menus finisched");
	}

	void OnLevelWasLoaded(int level)
	{
		if (level == CommonGameObjects.GameController.GetComponent<SceneController>().scene_MainMenu)
		{
			CloseAllMenus();
			ShowMenu(MainMenu);
		}
		else
		{
			HideMenuInstantly();
		}
	}
	#endregion
	#region Methods for Buttons
	public void ExitGame()
	{
		Debug.Log("Quit Game");
		Application.Quit();
	}
	public void PlayButtonClickSound()
	{
		audioSrc_Component.clip = ButtonClickSound.audioClip;
		audioSrc_Component.outputAudioMixerGroup = ButtonClickSound.audioMixerGroup;
		audioSrc_Component.loop = false;
		audioSrc_Component.spatialBlend = 0f;
		audioSrc_Component.Play();
	}

	public void Application_LoadMainMenu()
	{
		CommonGameObjects.GameController.GetComponent<SceneController>().OpenMainMenu();
	}
	public void Application_LoadLevel(int chapter)
	{
		CommonGameObjects.GameController.GetComponent<SceneController>().OpenLevel(chapter, 0);
	}
    public void Application_LoadScene(int scene)
    {
        CommonGameObjects.GameController.GetComponent<SceneController>().LoadLevel(scene);
    }
	public void Application_RestartLevel()
	{
		CommonGameObjects.GameController.GetComponent<SceneController>().RestartLevel();
	}
	#endregion

	#region Nested
	[System.Serializable]
	public class AudioSetting
	{
		public AudioClip audioClip;
		public UnityEngine.Audio.AudioMixerGroup audioMixerGroup;
	}
	#endregion
}

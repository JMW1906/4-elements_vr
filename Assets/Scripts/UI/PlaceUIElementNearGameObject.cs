﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(RectTransform))]
public class PlaceUIElementNearGameObject : MonoBehaviour {

	static PlaceUIElementNearGameObject m_Instance;
	public static PlaceUIElementNearGameObject Instance {
		get{ return m_Instance; }
		private set{
			if (m_Instance == null)
				m_Instance = value;
			else
				Debug.LogError ("There Can't be 2 Instances of PlaceUIElementNearGameObject!");
		}
	}

	public GameObject Target;
	[Tooltip("Wenn UI == null wird dieses Objekt benutzt")]
	public RectTransform UI;
	public UnityEngine.UI.Text Text;
	public Vector3 UI_Rotation;

	void Awake()
	{
		Instance = this;
		UI.gameObject.SetActive (false);
	}

	void Start()
	{
		if (UI == null) {
			UI = (RectTransform)(this.transform);
		}
		if (Text == null) {
			Text = UI.GetComponentInChildren<UnityEngine.UI.Text> ();
		}
	}


	void Update () {
		if (Target != null)
			FollowTarget ();
	}
	void FollowTarget(){
		Vector3 TargetScreenPosition = Camera.main.WorldToScreenPoint (Target.transform.position);
		Vector3 UI_NewPosition = new Vector3();

		UI_NewPosition.y = TargetScreenPosition.y;
		UI_NewPosition.z = 0;
		if (TargetScreenPosition.x - Screen.width / 2 < 0) {
			//Links
			UI_NewPosition.x = TargetScreenPosition.x + UI.sizeDelta.x / 2;
			UI.localRotation = Quaternion.Euler (UI_Rotation);
		} else {
			//Rechts
			UI_NewPosition.x = TargetScreenPosition.x - UI.sizeDelta.x / 2;
			UI.localRotation = Quaternion.Euler (UI_Rotation.x, -UI_Rotation.y, UI_Rotation.z);
		}
		UI.localPosition = UI_NewPosition;
	}

	public void SetTarget(GameObject target, string text=""){
		if (target == null)
			UI.gameObject.SetActive (false);
		else {
			UI.gameObject.SetActive (true);
			Text.text = text;
		}
		this.Target = target;
	}
}

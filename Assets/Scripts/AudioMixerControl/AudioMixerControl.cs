﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class AudioMixerControl : MonoBehaviour {
	public const string IngameVol = "IngameVol";
	public const string MasterVol = "MasterVol";
	public const string MusikVol = "MusikVol";
	public const string SoundEffectsVol = "SoundEffectsVol";

	public AudioMixer masterMixer;

	public static AudioMixerControl Instance;

	void Awake(){
		Instance = this;
		
	}

//Master Volume
	public void SetMasterVolumeLvl(float vol){
		masterMixer.SetFloat(MasterVol, vol);
		Debug.Log ("Master Volume set to " + vol);
	}
	public void GetMasterVolumeLvl(out float vol){
		masterMixer.GetFloat(MasterVol, out vol);
	}

//Musik Volume
	public void SetMusikVolumeLvl(float vol){
		masterMixer.SetFloat(MusikVol, vol);
		Debug.Log ("Music Volume set to " + vol);
	}
	public void GetMusikVolumeLvl(out float vol){
		masterMixer.GetFloat(MusikVol, out vol);
	}

//SoundEffect Volume
	public void SetSoundEffectVolumeLvl(float vol){
		masterMixer.SetFloat(SoundEffectsVol, vol);
		Debug.Log ("SFX Volume set to " + vol);
	}
	public void GetSoundEffectVolumeLvl(out float vol){
		masterMixer.GetFloat(SoundEffectsVol, out vol);
	}
	public void ToggleIngameVol(bool enabled){
		if (enabled){
			masterMixer.SetFloat(IngameVol, 0f);
			Debug.Log ("Ingame Volume enabled");
		}
		else{
			masterMixer.SetFloat(IngameVol, -80f);
			Debug.Log ("Ingame Volume Muted");
		}
	}
}

﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct MinMax_float {

	public float Min;
	public float Max;
}

[System.Serializable]
public struct MinMax_Rotation {

	[Range(-180, 180)] public float Min;
	[Range(-180, 180)] public float Max;

	public float Difference{
		get{
			return (Max - Min);
		}
	}
	public float Average{
		get{
			return ((Max + Min)/2);
		}
	}
}
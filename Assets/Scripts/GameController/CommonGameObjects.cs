﻿using UnityEngine;
using System.Collections;

public class CommonGameObjects : MonoBehaviour {

	#region Fields
	[SerializeField]
	private static GameObject m_Player;
	[SerializeField]
	private static GameObject m_GameController;
	[SerializeField]
	private static GameObject m_UI;
	#endregion
	#region Properties
	public static GameObject Player
	{
		get
		{
			if (m_Player == null)
				m_Player = GameObject.FindGameObjectWithTag(Tags.Player);
			return m_Player;
		}
		set{m_Player = value;}
	}
	public static GameObject GameController
	{
		get
		{
			return m_GameController;
		}
		set{m_GameController = value;}
	}
	public static GameObject UI
	{
		get
		{
			if (m_UI == null)
				m_UI = GameObject.FindGameObjectWithTag(Tags.UI);
			return m_UI;
		}
		set{m_UI = value;}
	}
	#endregion

	void Awake () {
		Player = Player;
		GameController = gameObject;
		UI = UI;
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {


	public int scene_MainMenu = 0;
	public int[] firstScenesInLevel;    //Last Index is the first levelID after the last level of the last Chapter

	[Space()]
	public int actuelleScene;


	void Start () {
		LevelChanged();
	}

	public void OpenMainMenu(){
		LoadLevel(scene_MainMenu);
	}
	public void OpenLevel(int chapter, int level){
		if (chapter < firstScenesInLevel.Length - 1){
			int sceneCountInChapter = firstScenesInLevel[chapter + 1] - firstScenesInLevel[chapter];
			if (level < sceneCountInChapter){
				int sceneID = firstScenesInLevel[chapter] + level;
				LoadLevel(sceneID);
			}
			else{
				if (sceneCountInChapter != 0)
					Debug.LogError("SceneController: OpenLevel: int level("+level+") is to Big! Value between 0 and "+(sceneCountInChapter-1)+" expected!");
				else
					Debug.LogError("SceneController: OpenLevel: There is no Level in this Chapter ("+chapter+")");
			}
		}
		else{
			Debug.LogError("SceneController: OpenLevel: int chapter("+chapter+") is to Big! Value between 0 and "+(firstScenesInLevel.Length - 2)+" expected!");
		}
	}

	void LevelChanged(){
		actuelleScene = SceneManager.GetActiveScene().buildIndex;
	}
    public void LoadLevel(int ID){
        actuelleScene = ID;
		SceneManager.LoadScene(ID);
		//LevelChanged();
		Debug.Log("SceneController: LoadLevel: Level Loaded(ID="+ID+")");
	}
	public void RestartLevel()
	{
		Debug.Log ("Restarting Level");
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public enum Chapter {Feuer, Luft, Wasser, Erde}
}

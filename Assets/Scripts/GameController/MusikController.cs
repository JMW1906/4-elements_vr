﻿using UnityEngine;
using System.Collections;

public class MusikController : MonoBehaviour {

	public int MinTimeBetweenClips = 0;
	public int MaxTimeBetweenClips = 0;
	public bool UseRandomClipOrder = true;
	public AudioClip[] AudioClips;
	public int SelectedAudioClipIndex = 0;

	private AudioSource audioSource;
	private int TimeLeftUntilNextClip;

	void Start () {
		audioSource = gameObject.GetComponent<AudioSource>();
	}
	
	void Update () {
		if (audioSource.time == 0) {
			if (TimeLeftUntilNextClip <= 0) {
				PlayNextAudioClip();
				TimeLeftUntilNextClip = Random.Range(MinTimeBetweenClips, MaxTimeBetweenClips);
			}
			TimeLeftUntilNextClip--;
		}
	}
	void PlayNextAudioClip() {
		GetNextAudioClipIndex();
		audioSource.clip = AudioClips[SelectedAudioClipIndex];
		audioSource.Play();
		Debug.Log("Started Playing the music: " + AudioClips [SelectedAudioClipIndex]);
	}
	void GetNextAudioClipIndex() {
		if (UseRandomClipOrder) {
			int random = Random.Range(1, AudioClips.GetLength(0) - 1);
			if (random <= SelectedAudioClipIndex)
				random--;
			SelectedAudioClipIndex = random;
		}
		else {
			SelectedAudioClipIndex++;
			if (SelectedAudioClipIndex >= AudioClips.GetLength(0)){
				SelectedAudioClipIndex = 0;
			}
		}

	}
}

using UnityEngine;
using System.Collections;

public class Torchelight : MonoBehaviour {
	
	public GameObject TorchLight;
	public GameObject MainFlame;
	public GameObject BaseFlame;
	public GameObject Etincelles;
	public GameObject Fumee;
	public float MaxLightIntensity;
	public float IntensityLight;
	
    private ParticleSystem.EmissionModule MainFlame_Emmision;

	void Start () {
//        MainFlame_Emmision = MainFlame.GetComponent<ParticleSystem>().emission;
        //MainFlame_Emmision.rate = IntensityLight*20f;
//        SetEmissionRate(ref MainFlame_Emmision, IntensityLight*20f);

		TorchLight.GetComponent<Light>().intensity=IntensityLight;
		//MainFlame.GetComponent<ParticleSystem>().emissionRate=IntensityLight*20f;
//        BaseFlame.GetComponent<ParticleSystem>().emission.rate=IntensityLight*15f;	
//        Etincelles.GetComponent<ParticleSystem>().emission.rate=IntensityLight*7f;
//        Fumee.GetComponent<ParticleSystem>().emission.rate=IntensityLight*12f;
	}
	

	void Update () {
		if (IntensityLight<0) IntensityLight=0;
		if (IntensityLight>MaxLightIntensity) IntensityLight=MaxLightIntensity;		

		TorchLight.GetComponent<Light>().intensity=IntensityLight/2f+Mathf.Lerp(IntensityLight-0.1f,IntensityLight+0.1f,Mathf.Cos(Time.time*30));

		TorchLight.GetComponent<Light>().color=new Color(Mathf.Min(IntensityLight/1.5f,1f),Mathf.Min(IntensityLight/2f,1f),0f);
//        SetEmissionRate(ref MainFlame_Emmision, IntensityLight*20f);
//        MainFlame.GetComponent<ParticleSystem>().emission.rate=IntensityLight*20f;
//        BaseFlame.GetComponent<ParticleSystem>().emission.rate=IntensityLight*15f;
//        Etincelles.GetComponent<ParticleSystem>().emission.rate=IntensityLight*7f;
//        Fumee.GetComponent<ParticleSystem>().emission.rate=IntensityLight*12f;		

	}

//    void SetEmissionRate(ref ParticleSystem.EmissionModule emissionModule, float Rate)
//    {
//        emissionModule.rate.constantMax = Rate;
//        emissionModule.rate.constantMin = Rate;
//    }
}
